import "./App.css";
import Layout from "./controllers/Layout/Layout";
import { Switch, Route, Redirect } from "react-router-dom";
import Home from "./components/Home/Home";
import Logout from "./controllers/Auth/Logout/Logout";
import { useSelector, useDispatch } from "react-redux";
import { selectIsAuthenticated } from "./features/authSlice";
import { useEffect } from "react";
import { checkAuthState } from "./features/authSlice";
import asyncComponent from "./hoc/asyncComponent";

const asyncAuth = asyncComponent(() => import("./controllers/Auth/Auth"));
const asyncPlay = asyncComponent(() => import("./components/Play/Play"));
const asyncNotFound = asyncComponent(() =>
  import("./components/NotFound/NotFound")
);

function App() {
  useEffect(() => {
    dispatch(checkAuthState());
  }, []);
  const dispatch = useDispatch();
  const isAuthenticated = useSelector(selectIsAuthenticated);
  let routes = (
    <Switch>
      <Route path="/auth" component={asyncAuth} />
      <Route path="/notfound" component={asyncNotFound} />
      <Route path="/" exact component={Home} />
      <Redirect to="/notfound" />
    </Switch>
  );
  if (isAuthenticated) {
    routes = (
      <Switch>
        <Route path="/play" component={asyncPlay} />
        <Route path="/auth" component={asyncAuth} />
        <Route path="/logout" component={Logout} />
        <Route path="/notfound" exact component={asyncNotFound} />
        <Route path="/" exact component={Home} />
        <Redirect to="/notfound" />
      </Switch>
    );
  }
  return (
    <div className="App">
      <Layout>{routes}</Layout>
    </div>
  );
}

export default App;
