import React from "react";
import { useSelector } from "react-redux";
import { selectToken } from "../../features/authSlice";
export default function Home() {
  const token = useSelector(selectToken);
  return (
    <div>
      <p>Home</p>
    </div>
  );
}
