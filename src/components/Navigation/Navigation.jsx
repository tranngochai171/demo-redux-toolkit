import React from "react";
import NavigationItem from "./NavigationItem/NavigationItem";
import classes from "./Navigation.module.css";
import { useSelector } from "react-redux";
import { selectIsAuthenticated } from "../../features/authSlice";

const Navigation = (props) => {
  const isAuthenticated = useSelector(selectIsAuthenticated);
  return (
    <div className={classes.Nav}>
      <NavigationItem to="/">Home</NavigationItem>
      {isAuthenticated ? (
        <>
          <NavigationItem to="/play">Play</NavigationItem>
          <NavigationItem to="/logout">Logout</NavigationItem>
        </>
      ) : (
        <NavigationItem to="/auth">Authentication</NavigationItem>
      )}
    </div>
  );
};

export default Navigation;
