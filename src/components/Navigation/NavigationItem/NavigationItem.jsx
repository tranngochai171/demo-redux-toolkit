import React from "react";
import { NavLink } from "react-router-dom";
import PropTypes from "prop-types";
import classes from "./NavigationItem.module.css";

const navigationItem = (props) => (
  <div className={classes.NavigationItem}>
    <NavLink activeClassName={classes.active} to={props.to} exact>
      {props.children}
    </NavLink>
  </div>
);

navigationItem.propTypes = {
  to: PropTypes.string.isRequired,
};

export default navigationItem;
