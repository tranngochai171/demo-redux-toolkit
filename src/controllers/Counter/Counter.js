import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  selectCounter,
  increment,
  decrement,
  incrementByAmount,
  asyncIncrementByAmount,
} from "../../features/counterSlice";

const Counter = (props) => {
  const count = useSelector(selectCounter);
  const dispatch = useDispatch();
  const [amount, setAmount] = useState(2);
  return (
    <>
      <h1>Counter</h1>
      <p>{count}</p>
      <div>
        <input value={amount} onChange={(e) => setAmount(e.target.value)} />
        <button onClick={() => dispatch(increment())}>+</button>
        <button onClick={() => dispatch(decrement())}>-</button>
        <button onClick={() => dispatch(incrementByAmount(amount))}>
          Increment
        </button>
        <button onClick={() => dispatch(asyncIncrementByAmount(amount))}>
          Increment By Async
        </button>
      </div>
    </>
  );
};

export default Counter;
