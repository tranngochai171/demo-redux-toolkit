import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import Spinner from "../../components/UI/Spinner/Spinner";
import {
  selectIsLoading,
  asyncSignIn,
  selectError,
  selectIsAuthenticated,
} from "../../features/authSlice";
import classes from "./Auth.module.css";
import { Redirect } from "react-router-dom";

export default function Auth() {
  const dispatch = useDispatch();
  const isLoading = useSelector(selectIsLoading);
  const error = useSelector(selectError);
  const isAuthenticated = useSelector(selectIsAuthenticated);
  const [data, setData] = useState({
    email: "",
    password: "",
  });
  const [isAllow, setIsAllow] = useState(false);
  const handleOnchange = (e) => {
    const tempData = { ...data };
    tempData[e.target.name] = e.target.value;
    let isAllow = true;
    for (const item in tempData) {
      isAllow = !!tempData[item] && isAllow;
    }
    setData(tempData);
    setIsAllow(isAllow);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    if (isAllow) {
      dispatch(asyncSignIn(data));
    }
  };
  let form = (
    <form>
      <div>
        <input
          type="email"
          value={data.email}
          placeholder="Your email"
          name="email"
          onChange={handleOnchange}
        />
      </div>
      <div>
        <input
          type="password"
          value={data.password}
          placeholder="Your Password"
          name="password"
          onChange={handleOnchange}
        />
      </div>
      <div>
        <button onClick={handleSubmit} disabled={!isAllow}>
          Submit
        </button>
      </div>
    </form>
  );
  if (isLoading) {
    form = <Spinner />;
  }
  let redirect = null;
  if (isAuthenticated) {
    redirect = <Redirect to="/play" />;
  }
  return (
    <div className={classes.AuthForm}>
      {redirect}
      {error ? error : null}
      {form}
    </div>
  );
}
