import { createSlice } from "@reduxjs/toolkit";
import axios from "axios";

const initialState = {
  token: null,
  isLoading: false,
  error: null,
};

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    authInit: (state) => {
      state.isLoading = true;
      state.error = null;
    },
    authSuccess: (state, { payload }) => {
      state.isLoading = false;
      state.token = payload;
      state.error = null;
    },
    authFail: (state, { payload }) => {
      state.isLoading = false;
      state.error = payload;
    },
    authLogout: (state) => {
      localStorage.removeItem("token");
      localStorage.removeItem("expirationTime");
      state.token = null;
    },
  },
});

export const {
  authInit,
  authSuccess,
  authFail,
  authLogout,
} = authSlice.actions;

const setTimeOutForToken = (expiresIn) => (dispatch) => {
  setTimeout(() => {
    dispatch(authLogout());
  }, expiresIn * 1000);
};

export const asyncSignIn = (formData) => async (dispatch) => {
  dispatch(authInit());
  try {
    formData = { ...formData, returnSecureToken: true };
    const result = await axios.post(
      `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyBhhbiAfV_5QfhH_cJXA7JApfXsJkiUDqY`,
      formData
    );

    const expirationTime = new Date(
      new Date().getTime() + result.data.expiresIn * 1000
      // new Date().getTime() + 10 * 1000 // 10 seconds
    );

    localStorage.setItem("expirationTime", expirationTime);
    localStorage.setItem("token", result.data.idToken);
    dispatch(authSuccess(result.data.idToken));
    dispatch(setTimeOutForToken(result.data.expiresIn));
  } catch (error) {
    dispatch(authFail(error.response.data.error.message));
  }
};

export const checkAuthState = () => (dispatch) => {
  const token = localStorage.getItem("token");
  if (!token) {
    dispatch(authLogout());
  } else {
    const expirationTime = new Date(localStorage.getItem("expirationTime"));
    if (expirationTime <= new Date()) {
      dispatch(authLogout());
    } else {
      dispatch(authSuccess(token));
      dispatch(
        setTimeOutForToken(
          (expirationTime.getTime() - new Date().getTime()) / 1000
        )
      );
    }
  }
};

export const selectToken = (state) => state.auth.token;
export const selectIsLoading = (state) => state.auth.isLoading;
export const selectError = (state) => state.auth.error;
export const selectIsAuthenticated = (state) => !!state.auth.token;
export default authSlice.reducer;
